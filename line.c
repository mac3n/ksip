#include "ksep.h"
#include <string.h>
#include <assert.h>

/* Frame a line in the range around its midpoint.
 * Line starts, ends *after* EOL, iff present.
 */
range
midline(const char *text, range r, size_t mid) {
	if (!rempty(r)) {
		range l = r;
		const char *eol = memchr(text + mid, '\n', r.hi - mid);
		if (eol) {
			l.hi = eol + 1 - text;
		}
		// search backward for NL
		// should use memrchr if present
		for (l.lo = mid; r.lo < l.lo; l.lo--) {
			if (text[l.lo - 1] == '\n') {
				break;
			}
		}
		r = l;
	}
	return r;
}


/* Frame nth field.
 * EOL and tabs are NOT included.
 */
range
field(const char *text, range r, unsigned char tab, int fn) {
	if (rempty(r)) {
		return r;
	}
	if (text[r.hi - 1] == '\n') {
		// exclude EOL
		r.hi--;
	}
	if (fn != 0) {
		for (;;) {
			const char *t = memchr(text + r.lo, tab, rlen(r));
			if (--fn == 0) {
				// this field
				if (t) {
					// set end of field
					r.hi = t - text;
				}
				break;
			}
			// next field
			r.lo = t ? t - text + 1 : r.hi;
		}
	}
	return r;
}

#ifdef TEST // unit tests

static void
test_line(void) {
	const char test[] = "012345678\n012345678\n0123456789";
	range r = {0, sizeof test - 1}, s;
	// line in middle
	s = midline(test, r, 15);
	assert(s.lo == 10 && s.hi == 20);
	// line at start
	r.hi = 15;
	s = midline(test, r, 8);
	assert(s.lo == 0 && s.hi == 10);
	// start on eol
	s = midline(test, r, 9);
	assert(s.lo == 0 && s.hi == 10);
	// range with no eol
	r.hi = 9;
	s = midline(test, r, 5);
	assert(s.lo == 0 && s.hi == 9);
}

static void
test_field(void) {
	const char test[] = "012\t456\t89\n";
	range r = {0, sizeof test - 1}, s;
	// full line
	s = field(test, r, '\t', 0);
	assert(s.lo == r.lo && s.hi == 10);
	// first
	s = field(test, r, '\t', 1);
	assert(s.lo == 0 && s.hi == 3);
	// mid
	s = field(test, r, '\t', 2);
	assert(s.lo == 4 && s.hi == 7);
	// last
	s = field(test, r, '\t', 3);
	assert(s.lo == 8 && s.hi == 10);
	// none
	s = field(test, r, '\t', 4);
	assert(s.lo == s.hi);
	// no EOL
	r.hi -= 1;
	s = field(test, r, '\t', 0);
	assert(s.lo == r.lo && s.hi == r.hi);
	// last
	s = field(test, r, '\t', 3);
	assert(s.lo == 8 && s.hi == 10);
}

int
main(int argc, char *argv[]) {
	say(SAYTEST, "test line\n");
	test_line();
	say(SAYTEST, "test field\n");
	test_field();
}

#endif // TEST
