
CFLAGS=-Wall

ksep: diff.o line.o sect.o mmio.o say.c ksep.o

.c.o:	ksep.h

# unit tests

.SUFFIXES: .test

.c.test:	ksep.h
	cc $(CFLAGS) -DTEST -o $@ $< say.o
	./$@

sect.test:	sect.c line.o
	cc $(CFLAGS) -DTEST -o $@ $< line.o say.o
	./$@

test:	say.o diff.test line.test mmio.test sect.test
	echo "tests passed"

# maintenance

clean:
	-rm *.o *.test
