#include "ksep.h"
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <argp.h>

/* Report to user. */
int saymask = SAYDIE|SAYWARN|SAYTEST;
void
say(char mask, const char *why, ...) {
	va_list ap;
	va_start(ap, why);
	if (mask & saymask) {
		vfprintf(stderr, why, ap);
	}
	if (mask & SAYDIE) {
		exit(-1);
	}
	va_end(ap);
}
