
#include <stdlib.h>

/* A search range [lo .. hi-1]. */
typedef struct { size_t lo, hi; } range;
/* Length of range */
static inline size_t rlen(const range r) { return r.hi - r.lo; }
/* Test for an empty range. */
static inline int rempty(const range r) { return r.hi <= r.lo; }

/* Frame a line around midpoint. */
range midline(const char *text, range r, size_t mid);
/* rame field in line. */
range field(const char *text, range r, unsigned char tab, int fn);

/* Function to match line to key. */
typedef int kdiff(void *key, const char *text, range line);
/* Extend match range and flag disorder. */
typedef int extdiff(int *dis, int diff);
/* Binary search for a key match. */
range bsect(const char *text, range *r, void *key, kdiff *diff, extdiff *ext);
/* Extend key match to matching range. */
range rsect(const char *text, range r, void *key, kdiff *diff);

/* String diff, with options. */
enum {SKICASE=1, SKNUM=2, SKPFX=4};
int sdiff(const char *k, size_t klen, const char *f, size_t flen, char opts);
/* Decimal unsigned numeric compare in (inclusive) bounds. */
int ndiff(unsigned lwb, unsigned upb, const char *f, size_t flen);

/* Map a file. Returns a null address if it failed. */
range mmapfile(const char **text, const char *path);
/* Unmap the file, which we've already closed. */
void unmmapfile(const char **text, range r);
/* Write from map. */
void mmput(const char *text, range r);

/* Cheap error reporting. */
enum {
	// generic messages
	SAYDIE=1,
	SAYWARN=2,
	// tracing
	SAYTEST=4,
	SAYPROBES = 8,
	SAYDIFF=16,
	SAYSECT=32
};

extern int saymask;
void say(char mask, const char *why, ...);
