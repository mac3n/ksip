#include "ksep.h"
#include <string.h>
#include <argp.h>

/* parsed options */

// numeric key range
unsigned keylwb = 0, keyupb = 0;
// string key
static char *keystr = NULL;
static int keystrlen = 0;
// string key options
static int keystrflags = 0;
// comparison
static int (*keydiff)(const char *str, size_t len);
static int keydiffs = 0;
// file keys
static int keyfn = 0;
// field delimiter
static char keytab = '\t';

/* Handle numeric range diff. */
static int
keyndiff(const char *str, size_t len) {
	return ndiff(keylwb, keyupb, str, len);
}

/* Handle string diff. */
static int
keysdiff(const char *str, size_t len) {
	return sdiff(keystr, keystrlen, str, len, keystrflags);
}

/* Called to diff on a field in the line. */
static int
diff(void *key, const char *text, range r) {
	range f = field(text, r, keytab, keyfn);
	int diff = keydiff(text + f.lo, rlen(f));
	char cmp = diff < 0 ? '<' : diff > 0 ? '>' : '=';
	say(SAYDIFF, "diff %c \"%.*s\"\n", cmp, rlen(f), text + f.lo);
	keydiffs++;
	return diff;
}

/* Command line option and arg parsing */

/* Search file named in command arg. */
static error_t
parsefile(char *arg, struct argp_state *state) {
	const char *text = NULL;
	range r = mmapfile(&text, arg);
	range s = {0, 0};
	if (!text) {
		argp_error(state, "can't map: %s\n", arg);
		return EINVAL;
		return EINVAL;
	}
	if (!keydiff) {
		argp_error(state, "no search key specified\n");
		return EINVAL;
	}
	s = rsect(text, r, NULL, diff);
	if (!rempty(s)) {
		say(SAYSECT, "found [%lu:%lu]\n", s.lo, s.hi);
		mmput(text, s);
	}
	unmmapfile(&text, r);
	say(SAYPROBES, "%u probes\n", keydiffs);
	return 0;
}

/* Parse numeric key lo[-hi]. */
static error_t
parsenkey(char *arg, struct argp_state *state) {
	char *rest = NULL;
	keydiff = keyndiff;
	keyupb = keylwb = strtoul(arg, &rest, 10);
	if (!*rest) {
		// single value
		return 0;
	}
	if (*rest == '-') {
		// lwb<upb
		keyupb = strtoul(rest + 1, &rest, 10);
	}
	if (!*rest) {
		// clean
		if (keylwb > keyupb) {
			say(SAYWARN, "empty range %d > %d\n", keylwb, keyupb);
		}
		return 0;
	}
	argp_error(state, "not a numeric range: %s", arg);
	return EINVAL;
}

/* Parse field number */
static error_t
parsefn(char *arg, struct argp_state *state) {
	char *rest = NULL;
	keyfn = strtoul(arg, &rest, 10);
	if (!*rest) {
		return 0;
	}
	argp_error(state, "not a field number: %s", arg);
	return EINVAL;
}

/* Parse key string */
static error_t
parseskey(char *arg, struct argp_state *state) {
	keydiff = keysdiff;
	keystr = arg;
	keystrlen = strlen(arg);
	return 0;
}

/* Parse field delimiter char. */
static error_t
parsetab(char *arg, struct argp_state *state) {
	if (strlen(arg) == 1) {
		keytab = arg[0];
		return 0;
	}
	argp_error(state, "not a delimiter %s", arg);
	return EINVAL;
}

// verbosity
static int optsay[] = {SAYPROBES, SAYSECT, SAYDIFF, 0};
static int optv = 0;

// our CLI options
static struct argp_option opts[] = {
	{0, 0, 0, 0, "search keys:", 1},
	{"numeric", 'N', "INT[-INT]", 0, "numeric search. M-N matches a range."},
	{"string", 'S', "STRING", 0, "string search"},
	{0, 0, 0, 0, "file format:", 2},
	{"field", 'f', "FIELD", 0, "match on selected field (default is whole line)"},
	{"delimiter", 'd', "CHAR", 0, "field delimiter (default is TAB)"},
	{0, 0, 0, 0, "string matching", 3},
	{"prefix", 'p', 0, 0, "key matches as string prefix"},
	{"ignore-case", 'i', 0, 0, "strings match ignoring case"},
	{"numeric", 'n', 0, 0, "string ordered by numeric subfields"},
	{0, 0, 0, 0, "other:", 4},
	{"verbose", 'v', 0, 0, "increase verbosity (may be repeated)"},
	{0}
};

/* Called with argv options. */
static error_t
optsparse(int k, char *arg, struct argp_state *state) {
	switch (k) {
	case 'N':
		return parsenkey(arg, state);
	case 'S':
		return parseskey(arg, state);

	case 'f':
		return parsefn(arg, state);
	case 'd':
		return parsetab(arg, state);
	case 'i':
		keystrflags |= SKICASE;
		return 0;
	case 'n':
		keystrflags |= SKNUM;
		return 0;
	case 'p':
		keystrflags |= SKPFX;
		return 0;

	case 'v':
		if (optsay[optv]) {
			saymask |= optsay[optv];
			optv++;
		}
		return 0;

	case ARGP_KEY_ARG:
		parsefile(arg, state);
		return 0;
	case ARGP_KEY_END:
		if (!state->arg_num) {
			// no files
			say(SAYWARN, "no files to search\n");
		}
		return 0;
	case ARGP_KEY_INIT:
		return 0;
	default:
		return ARGP_ERR_UNKNOWN;
	};
}

static struct argp argp = {
	opts,
	optsparse,
	"FILE...",
	"Search sorted text files for key match."
};

int
main(int argc, char *argv[]) {
	argp_parse(&argp, argc, argv, 0, 0, NULL);
	return 0;
}

