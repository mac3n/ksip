#include "ksep.h"
#include <stdio.h>
#include <assert.h>

/* Binary search for a key match.
 * Uses diff ordering key - field.
 * Updates tightest nonmatching bounds.
 */
range
bsect(const char *text, range *r, void *key, kdiff *diff, extdiff *ext) {
	while (!rempty(*r)) {
		// find a line
		range l = midline(text, *r, r->lo + rlen(*r) / 2);
		say(SAYSECT, "bsect [%lu:%lu]\n", r->lo, r->hi);
		// compare to line
		int dis = 0;
		int d = ext(&dis, diff(key, text, l));
		if (dis) {
			say(SAYDIE, "disordered line %.*s\n", l.hi - l.lo, text + l.lo);
		}
		if (d < 0) {
			// key < field, go lower
			r->hi = l.lo;
		}
		else if (0 < d) {
			// key > field, go higher
			r->lo = l.hi;
		}
		else {
			// match
			return l;
		}
	}
	// not found
	say(SAYSECT, "bsect [%lu:%lu]\n", r->lo, r->hi);
	return *r;
}

/* Exact match. */
static int exteq(int *dis, int diff) {
	return diff;
}

/* Match until too low. */
static int
extlo(int *dis, int diff) {
	if (diff < 0) {
		// shouldn't be above key
		*dis = diff;
	}
	return diff == 0 ? -1 : diff;
}

/* Match until too high. */
static int exthi(int *dis, int diff) {
	return -extlo(dis, -diff);
}

/* Match key range.
 * Starting from one hit, search for the matching range limits.
 */
range
rsect(const char *text, range r, void *key, kdiff *diff) {
	range s = bsect(text, &r, key, diff, exteq);
	if (!rempty(s)) {
		// extend below and above
		range dn = {.lo=r.lo, .hi=s.hi};
		range up = {.lo=s.lo, .hi=r.hi};
		say(SAYSECT, "rsect dn[%lu:%lu]\n", dn.lo, dn.hi);
		bsect(text, &dn, key, diff, extlo);
		say(SAYSECT, "rsect up[%lu:%lu]\n", up.lo, up.hi);
		bsect(text, &up, key, diff, exthi);
		s.lo = dn.hi;
		s.hi = up.lo;
	}
	return s;
}

#ifdef TEST // unit tests

/* single-character diff for test */
static int
testdiff1(void *key, const char *text, range f) {
	return rempty(f) ? +1 : *(char*)key - text[f.lo];
}

static void
test_bsect(void) {
	const char test[] = "B-\nC-\nX-\nY-\n";
	// found
	for (int i = 0; i < 4; i++) {
		char *key = &"BCXY"[i];
		range r = {0, sizeof test - 1};
		range s = bsect(test, &r, key, testdiff1, exteq);
		assert(s.lo == 3 * i && s.hi == 3 * (i+1));
		assert(r.lo <= s.lo && s.hi <= r.hi);
	}
	// not found
	for (int i = 0; i < 4; i++) {
		char *key = &"AMNZ"[i];
		range r = {0, sizeof test - 1};
		range s = bsect(test, &r, key, testdiff1, exteq);
		assert(rempty(s));
		assert(rempty(r));
	}
}

static void
test_rsect(void) {
	const char test[] = "A\nB\nC\nC\nC\nZ\n";
	range r = {0, sizeof test - 1}, s;
	// extended range
	s = rsect(test, r, "C", testdiff1);
	assert(s.lo == 4 && s.hi == 10);
	// unextended range
	s = rsect(test, r, "B", testdiff1);
	// no range
	assert(s.lo == 2 && s.hi == 4);
	s = rsect(test, r, "M", testdiff1);
}

int
main(int argc, char *argv[]) {
	say(SAYTEST, "test bsect\n");
	test_bsect();
	say(SAYTEST, "test rsect\n");
	test_rsect();
}

#endif // TEST
