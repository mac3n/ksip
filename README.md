Binary search on sorted text files.

`Ksep` is a command-line tool, named to rhyme with the grep family of text searches.

`Ksep` uses a typical binary search,
except that it operates on ASCII text files.
After choosing a midpoint to probe,
it scans forward and backward to find the line boundaries.
Once a line matches, ksep extends the match range
by searching the file for non-matching lines before and after the match.

For binary search to work, the text file must be sorted.
Results for unsorted files are unpredictable.
If a search encounters keys out or order, it aborts,
but `ksep` tries hard to not look at most of the file.

`Ksep` memory-maps the search files, so it is fast even on large files,
but it cannot work with streamed files such as file decompressions.

`Ksep` uses a linear scan to find line and field boundaries,
so it's not fast if your file is a few very large lines.

Usage
-----

```
Usage: ksep [OPTION...] FILE...
Search sorted text files for key match.

 search keys:
  -N, --numeric=INT[-INT]    numeric search. M-N matches a range.
  -S, --string=STRING        string search

 file format:
  -d, --delimiter=CHAR       field delimiter (default is TAB)
  -f, --field=FIELD          match on selected field (default is whole line)

 string matching
  -i, --ignore-case          strings match ignoring case
  -n, --numeric              string ordered by numeric subfields
  -p, --prefix               key matches as string prefix

 other:
  -v, --verbose              increase verbosity (may be repeated)
```

Building
--------

```
make test && make
```
