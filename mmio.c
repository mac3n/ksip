#include "ksep.h"
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <assert.h>

// something that is not NULL
static const char *none = "";

/* Map a file.
 * Returns a null address if it failed.
 */
range
mmapfile(const char **text, const char *path) {
	int fd;
	struct stat stat;
	range r = {0, 0};
	*text = NULL;
	// open and stat
	fd = open(path, O_RDONLY);
	if (fd < 0) {
		say(SAYDIE, "%s: %s\n", path, strerror(errno));
	}
	if (fstat(fd, &stat)) {
		say(SAYDIE, "%s: %s\n", path, strerror(errno));
	}
	// check for nonempty regular file (needed for mmap?)
	if (stat.st_size == 0) {
		// don't mmap empty
		*text = none;
	}
	else if ((stat.st_mode & S_IFMT) != S_IFREG) {
		say(SAYWARN, "%s: not a regular file\n", path);
	}
	else {
		r.hi = stat.st_size;
		*text = mmap(NULL, r.hi, PROT_READ, MAP_SHARED, fd, 0);
		if (*text == MAP_FAILED) {
			say(SAYDIE, "%s: %s\n", path, strerror(errno));
		}
	}
	// don't use the file handle
	close(fd);
	return r;
}

/* Unmap the file, which we've already closed. */
void
unmmapfile(const char **text, range r) {
	if (*text) {
		if (r.hi) {
			int err = munmap((void *)*text, r.hi);
			if (err) {
				say(SAYDIE, "unmap: %s\n", strerror(errno));
			}
		}
		*text = NULL;
	}
}

/* Write from mmapped file.
 * We don't bother with stdio buffers, but write directly from the mmap.
 */
void
mmput(const char *text, range r) {
	size_t size = rlen(r);
	size_t w = write(STDOUT_FILENO, text + r.lo, size);
	if (w < size) {
		say(SAYDIE, "write failed: %d", w);
	}
}

#ifdef TEST // unit tests

static void
test_mmap(void) {
	const char *test = NULL;
	range r;
	// should fail
	r = mmapfile(&test, "/etc");
	assert(!test);
	// should map
	r = mmapfile(&test, "/etc/passwd");
	assert(test);
	unmmapfile(&test, r);
	assert(!test);
	// don't map
	r = mmapfile(&test, "/dev/null");
	assert(test);
	unmmapfile(&test, r);
	assert(!test);
}

static void
test_mmput(void) {
	const char *test = "XXXok\nYYY";
	range r = {3, 6};
	mmput(test, r);
}

int
main(int argc, char *argv[]) {
	say(SAYTEST, "test mmap\n");
	test_mmap();
	say(SAYTEST, "test mmput\n");
	test_mmput();
}

#endif // TEST


