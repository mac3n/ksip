#include "ksep.h"
#include <ctype.h>
#include <assert.h>

/* Compares use strcmp ordering K:F returns K-F
 * The compare may be asymmetric, with key being a range.
 */


/* String diff,
 * with options for case-independence,
 * numeric field "versions" (see sort),
 * and key as prefix.
 */
int
sdiff(const char *k, size_t klen, const char *f, size_t flen, char opts) {
	const char *keos = k + klen, *feos = f + flen;
	// numeric substrings
	int kn = 0, fn = 0;
	// signed diff
	int diff = 0;
	while ((k < keos) && (f < feos)) {
		// accumulate numeric fields
		if ((opts & SKNUM) && isdigit(*k)) {
			kn = 10 * kn + *k++ - '0';
			continue;
		}
		if ((opts & SKNUM) && isdigit(*f)) {
			fn = 10 * fn + *f++ - '0';
			continue;
		}
		if ((diff = kn - fn)) {
			return diff;
		}
		// non-numeric
		kn = fn = 0;
		diff = (opts & SKICASE) ? tolower(*k) - tolower(*f) : *k - *f;
		if (diff) {
			return diff;
		}
		f++;
		k++;
	}
	// ended numeric?
	if ((diff = kn - fn)) {
		return diff;
	}
	if (k < keos) {
		// leftover key is later
		return +1;
	}
	if (f < feos) {
		// leftover field is later
		return (opts & SKPFX) ? 0 : -1;
	}
	return 0;
}

/* Decimal unsigned numeric compare in (inclusive) bounds.
 * Uses unsigned ints, suitable for timestamps or line numbers.
 */
int
ndiff(unsigned lwb, unsigned upb, const char *f, size_t flen) {
	const char *feos = f + flen;
	unsigned n = 0;
	while (f < feos) {
		if (!isdigit(*f)) {
			say(SAYDIE, "non-numeric field\n");
		}
		n = n * 10 + *f++ - '0';
	}
	return (n < lwb) ? lwb - n : (upb < n) ? upb - n : 0;
}

#ifdef TEST // unit tests

static void
test_ndiff(void) {
	// ignore text following field
	assert(ndiff(0, 1, "03", 1) == 0);
	// below bounds
	assert(ndiff(1, 2, "0", 1) > 0);
	// in bounds
	assert(ndiff(1, 2, "1", 1) == 0);
	assert(ndiff(1, 2, "2", 1) == 0);
	// above bounds
	assert(ndiff(1, 2, "3", 1) < 0);
	// multidigit
	assert(ndiff(10, 10, "10", 2) == 0);
}

static void
test_sdiff(void) {
	// K == K
	assert(sdiff("K1", 1, "K2", 1, 0) == 0);
	// K < KA
	assert(sdiff("K", 1, "KA", 2, 0) < 0);
	// KA > K
	assert(sdiff("KA", 2, "K", 1, 0) > 0);
	// K == K
	assert(sdiff("KA", 1, "K", 1, 0) == 0);
	// K* == KA
	assert(sdiff("K", 1, "KA", 2, SKPFX) == 0);
	// K10 > K1A
	assert(sdiff("K10", 3, "K1A", 3, SKNUM) > 0);
	// K == k
	assert(sdiff("K", 1, "k", 1, SKICASE) == 0);
}

int
main(int argc, char *argv[]) {
	say(SAYTEST, "test ndiiff\n");
	test_ndiff();
	say(SAYTEST, "test sdiiff\n");
	test_sdiff();
}

#endif // TEST
